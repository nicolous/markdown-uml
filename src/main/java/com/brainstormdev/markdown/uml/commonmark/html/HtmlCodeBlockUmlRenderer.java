package com.brainstormdev.markdown.uml.commonmark.html;

import java.io.IOException;
import java.util.Base64;
import java.util.Base64.Encoder;
import java.util.Collections;
import java.util.Map;
import java.util.Set;

import org.commonmark.node.FencedCodeBlock;
import org.commonmark.node.Node;
import org.commonmark.renderer.NodeRenderer;
import org.commonmark.renderer.html.HtmlNodeRendererContext;
import org.commonmark.renderer.html.HtmlWriter;

import com.brainstormdev.markdown.uml.BuildListener;
import com.brainstormdev.markdown.uml.UmlGenerator;


/**
 * Transform markdown's indented code blocks into HTML "img" tag.
 */
final class HtmlCodeBlockUmlRenderer implements NodeRenderer
{
    /** Base64 encoder used to output SVG into the src attribute of img tag. */
    private final Encoder base64Encoder;

    /** CommonMark's HTML renderer context. Access to the HTML writer. */
    private final HtmlNodeRendererContext context;

    /** UML image generator. */
    private final UmlGenerator umlGenerator;

    /** Build progress listener. */
    private final BuildListener listener;

    /**
     * Renderer using a specific UML generator.
     * @param context CommonMark's HTML renderer context.
     * @param umlGenerator UML image generator used to transform indented code blocks.
     * @param listener Build progress listener.
     */
    public HtmlCodeBlockUmlRenderer(final HtmlNodeRendererContext context, final UmlGenerator umlGenerator,
                                    final BuildListener listener)
    {
        this.context = context;
        this.umlGenerator = umlGenerator;
        this.listener = listener;
        base64Encoder = Base64.getEncoder();
    }

    @Override
    public Set<Class<? extends Node>> getNodeTypes()
    {
        return Collections.singleton(FencedCodeBlock.class);
    }

    @Override
    public void render(final Node node)
    {
        final HtmlWriter writer = context.getWriter();
        writer.line();

        final FencedCodeBlock codeBlock = (FencedCodeBlock) node;
        final String infoString = codeBlock.getInfo();
        final String code = codeBlock.getLiteral();
        final boolean umlRendered = renderUml(writer, infoString, code);

        if (!umlRendered)
        {
            writePreCode(code, writer);
        }

        writer.line();
    }

    /**
     * Generate HTML code from UML code.
     * @param writer CommonMark's HTML writer.
     * @param infoString <a href="https://spec.commonmark.org/0.28/#info-string">info string</a>
     * as described in the commonmark's spec.
     * @param code UML code.
     * @return true if the rendered has succeeded. See output log otherwise.
     */
    private boolean renderUml(final HtmlWriter writer, final String infoString, final String code)
    {
        final boolean umlRendered;
        if (umlGenerator.isInfoStringSupported(infoString) && umlGenerator.isSupported(code))
        {
            umlRendered = writeUmlImage(code, writer);
        }
        else
        {
            umlRendered = false;
        }
        return umlRendered;
    }

    /**
     * Write the "img" HTML tag corresponding to the UML code, using {@link #context}.
     * @param code UML to transform.
     * @param writer CommonMark's HTML writer.
     */
    private boolean writeUmlImage(final String code, final HtmlWriter writer)
    {
        String htmlImgData = null;
        try
        {
            final String umlBase64Png = umlToBase64Svg(code);
            htmlImgData = "data:image/svg+xml;base64," + umlBase64Png;
        }
        catch (final IOException e)
        {
            // Doesn't rethrow as #render() can't throw any checked exception.
            listener.onNoThrowError("Error occured during UML generation", e);
        }
        final boolean success = htmlImgData != null;
        if (success)
        {
            final Map<String, String> imgAttr = Collections.singletonMap("src", htmlImgData);
            writer.tag("img", imgAttr);
            writer.tag("/img");
        }
        return success;
    }

    /**
     * Write the code into pre/code tags.
     * @param code Code from the markdown file.
     * @param writer CommonMark's HTML writer.
     */
    private void writePreCode(final String code, final HtmlWriter writer)
    {
        writer.tag("pre");
        writer.tag("code");
        writer.text(code);
        writer.tag("/pre");
        writer.tag("/code");
    }

    /**
     * Transform text UML diagram to SVG/XML/base64.
     * @param uml UML to transform.
     * @return SVG XML as a base64 string.
     * @throws IOException An error occurred during the SVG file creation.
     */
    private String umlToBase64Svg(final String uml) throws IOException
    {
        final byte[] umlSvg = umlGenerator.toSvg(uml);
        final String umlBase64Png;
        umlBase64Png = base64Encoder.encodeToString(umlSvg);
        return umlBase64Png;
    }
}
