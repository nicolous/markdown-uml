package com.brainstormdev.markdown.uml.commonmark.html;

import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Path;

import org.commonmark.node.Node;
import org.commonmark.parser.Parser;
import org.commonmark.renderer.NodeRenderer;
import org.commonmark.renderer.Renderer;
import org.commonmark.renderer.html.HtmlNodeRendererContext;
import org.commonmark.renderer.html.HtmlNodeRendererFactory;
import org.commonmark.renderer.html.HtmlRenderer;

import com.brainstormdev.markdown.uml.BuildListener;
import com.brainstormdev.markdown.uml.UmlGenerator;

/**
 * Markdown to HTML document transformer.
 * <p>
 * Output HTML documents are encoding to UTF-8.
 * <p>
 * UML diagrams have to be defined in markdown's indented code blocks. The way UML diagram are generated is determined
 * by the {@link UmlGenerator} injected.
 */
public final class HtmlGenerator
{
    /** Markdown files parser. */
    private final Parser markdownParser;

    /** HTML files renderer. */
    private final Renderer htmlRenderer;

    /** Build progress listener. */
    private final BuildListener listener;

    /**
     * HTML generator with specific UML transformer.
     * @param umlGenerator Text to UML transformer.
     * @param listener Build progress listener.
     */
    public HtmlGenerator(final UmlGenerator umlGenerator, final BuildListener listener)
    {
        this.listener = listener;
        final HtmlRenderer.Builder htmlBuilder = HtmlRenderer.builder();
        final HtmlNodeRendererFactory umlRenderer = new HtmlNodeRendererFactory()
        {
            @Override
            public NodeRenderer create(final HtmlNodeRendererContext context)
            {
                return new HtmlCodeBlockUmlRenderer(context, umlGenerator, listener);
            }
        };
        htmlRenderer = htmlBuilder.nodeRendererFactory(umlRenderer).build();
        markdownParser = Parser.builder().build();
    }

    /**
     * Generate an HTML document from a markdown file.
     * <p>
     * Source file's directory tree is respected.
     * @param srcFile Input markdown file path.
     * @param targetDir Output directory for HTML files.
     * @throws IOException An error occurred either during the markdown parsing, the UML diagram generation, or the
     * generation of the HTML document.
     */
    public Path generate(final Path srcFile, final Path targetDir) throws IOException
    {
        final String srcFilename = srcFile.getFileName().toString();
        final Path outputDir = targetDir.getParent().resolve(srcFile.getParent());
        final Path outputFile = outputDir.resolve(srcFilename + ".html");

        listener.onFileBuildStart(srcFile, outputFile);
        outputDir.toFile().mkdirs();

        try (final FileWriter out = new FileWriter(outputFile.toFile());
                        final FileInputStream in = new FileInputStream(srcFile.toFile());
                        final InputStreamReader inputStreamReader = new InputStreamReader(in))
        {
            out.write("<!DOCTYPE html>\n" + //
                            "<html lang=\"fr\"><head><meta charset=\"utf-8\"/></head>\n" + //
                            "<body>\n");

            final Node document = markdownParser.parseReader(inputStreamReader);
            htmlRenderer.render(document, out);

            out.write("</body>\n" + //
                            "</html>");
            out.flush();
        }
        listener.onFileBuildEnd(outputDir, outputFile);

        return outputFile;
    }
}
