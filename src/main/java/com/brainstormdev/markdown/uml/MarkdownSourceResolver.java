package com.brainstormdev.markdown.uml;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Markdown source file resolver.
 * <p>
 * Recursively looks for .md files in a directory.
 */
public final class MarkdownSourceResolver
{
    /**
     * Get sources files recursively from a path.
     * @param srcPath String path of the directory where sources files are searched.
     * @return Sources files pathes found in the directory and sub-directories.
     * @throws IOException An error occurred when accessing the directory.
     */
    public List<Path> getSourceFiles(final Path srcPath) throws IOException
    {
        try (final Stream<Path> filesWalker = Files.walk(srcPath))
        {
            final Stream<Path> markdownFilesFilter = filesWalker.filter(Files::isRegularFile)
                            .filter(this::isMarkdownFile);
            final List<Path> mdFiles = markdownFilesFilter.collect(Collectors.toList());
            return mdFiles;
        }
    }

    /**
     * Test whether the file path corresponds to a markdown file.
     * @param path File path to test.
     * @return true if the file is a markdown file.
     */
    private boolean isMarkdownFile(final Path path)
    {
        final File file = path.toFile();
        final String fileName = file.getName();
        return fileName.toLowerCase().endsWith(".md");
    }
}
