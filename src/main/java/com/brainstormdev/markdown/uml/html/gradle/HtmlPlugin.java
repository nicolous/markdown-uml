package com.brainstormdev.markdown.uml.html.gradle;

import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.tasks.TaskContainer;

/**
 * Plugin that add tasks to convert marddown files to HTML documents.
 */
final class HtmlPlugin implements Plugin<Project>
{

    @Override
    public void apply(final Project project)
    {
        addHtmlTask(project);
    }

    /**
     * Create a task to convert markdown files to HTML document, with support for PlantUML diagram.
     * @param project Current project.
     */
    private void addHtmlTask(final Project project)
    {
        final TaskContainer tasksContainer = project.getTasks();

        final HtmlTaskType htmlTask = tasksContainer.create("md-to-html", HtmlTaskType.class);
        htmlTask.setGroup("Markdown to HTML");
        htmlTask.setSrcDir("src");
        htmlTask.setTargetDir("build");
        htmlTask.setDescription(
            "Generate an HTML document for each markdown file, with support for PlantUML diagram.\n" +
            "PlantUML code has to be in indented code blocks. The string @startuml is used to identify the text code " +
            "to transform. The whole indented text will be given the PlantUML parser.\n" +
            "PlantUML text is expected to be UTF-8 encoded.\n" +
            "The output is a stand-alone UTF-8 encoded HTML document. All UML diagrams generated are embedded as SVG " +
            "images.");
    }
}
