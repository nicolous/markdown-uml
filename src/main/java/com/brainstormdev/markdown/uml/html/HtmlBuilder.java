/**
 *
 */
package com.brainstormdev.markdown.uml.html;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

import com.brainstormdev.markdown.uml.BuildListener;
import com.brainstormdev.markdown.uml.MarkdownSourceResolver;
import com.brainstormdev.markdown.uml.commonmark.html.HtmlGenerator;
import com.brainstormdev.markdown.uml.plantuml.PlantUmlGenerator;

/**
 * Finds the markdown files to read, and transforms them HTML documents.
 *
 */
public final class HtmlBuilder
{
    /** CommonMark's markdown parser and HTML generator. */
    private final HtmlGenerator htmlGenerator;

    /** Resolver used to find all markdown source files in the input directory. */
    private final MarkdownSourceResolver markdownSrcResolver;

    /** Build progress listener. */
    private final BuildListener listener;

    /**
     * @param listener Build progress listener.
     */
    public HtmlBuilder(final BuildListener listener)
    {
        this.listener = listener;
        markdownSrcResolver = new MarkdownSourceResolver();
        final PlantUmlGenerator umlGenerator = new PlantUmlGenerator();
        htmlGenerator = new HtmlGenerator(umlGenerator, listener);
    }

    /**
     * Transform each markdown file to an HTML document.
     * @param srcDir Directory that contains the markdown source files.
     * @param targetDir Output directory for HTML files.
     * @throws IOException An error occurred during the source files listing or the output file creation.
     */
    public void build(final Path srcDir, final Path targetDir) throws IOException
    {
        final List<Path> srcFiles = markdownSrcResolver.getSourceFiles(srcDir);
        listener.onSourceDiscovery(srcFiles);

        for (final Path src : srcFiles)
        {
            htmlGenerator.generate(src, targetDir);
        }
    }
}
