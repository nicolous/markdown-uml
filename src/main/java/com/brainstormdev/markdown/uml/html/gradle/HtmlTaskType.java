package com.brainstormdev.markdown.uml.html.gradle;

import com.brainstormdev.markdown.uml.BuildListener;
import com.brainstormdev.markdown.uml.html.HtmlBuilder;
import org.gradle.api.DefaultTask;
import org.gradle.api.GradleScriptException;
import org.gradle.api.logging.Logger;
import org.gradle.api.tasks.InputDirectory;
import org.gradle.api.tasks.OutputDirectory;
import org.gradle.api.tasks.TaskAction;
import org.gradle.api.tasks.options.Option;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Task that generates an HTML document for each markdown file, with support for PlantUML diagram.
 * <p>
 * PlantUML code has to be in indented code blocks. The string {@code @startuml} is used to identify the text code to
 * transform. The whole indented text will be given the PlantUML parser. <br>
 * PlantUML text is expected to be UTF-8 encoded.
 * <p>
 * The output is a stand-alone UTF-8 encoded HTML document. All UML diagrams generated are embedded as SVG images.
 */
public class HtmlTaskType extends DefaultTask
{
    /**
     * Directory that contains the markdown source files.
     */
    @InputDirectory
    private String srcDir = ".";

    /**
     * Output directory for HTML files.
     */
    @OutputDirectory
    private String targetDir = ".";

    @Option(option = "mdSrcDir", description = "Directory that contains the markdown source files.")
    void setSrcDir(final String srcDir)
    {
        this.srcDir = srcDir;
    }

    @Option(option = "mdTargetDir", description = "Output directory for HTML files.")
    void setTargetDir(final String targetDir)
    {
        this.targetDir = targetDir;
    }

    @TaskAction
    void action()
    {
        final List<Exception> errors = new ArrayList<>();
        final HtmlBuilder builder = new HtmlBuilder(createBuildListener(errors));

        final Logger logger = getLogger();
        final URI srcDirUri = new File(srcDir).toURI();
        final URI targetDirUri = new File(targetDir).toURI();
        try
        {
            builder.build(Paths.get(srcDirUri), Paths.get(new File(targetDirUri).toURI()));
        }
        catch (final IOException e)
        {
            throw new GradleScriptException(e.getMessage(), e);
        }

        if (!errors.isEmpty())
        {
            for (final Exception exception : errors)
            {
                logger.error(exception.getMessage(), exception);
            }
            throw new GradleScriptException("See previous exceptions", null);
        }
    }

    /**
     * Create a listener for the build.
     * @param errors List that will be filled with the no-throw errors (see {@link BuildListener#onNoThrowError(String,
     * Exception)).
     * @return A listener that logs output with {@link Logger} and accumulates no-throw errors.
     */
    private BuildListener createBuildListener(final List<Exception> errors)
    {
        final Logger logger = getLogger();
        return new BuildListener()
        {

            @Override
            public void onSourceDiscovery(final List<Path> sourceFiles)
            {
                logger.info("Found " + sourceFiles.size() + " markdown files in " + srcDir);
            }

            @Override
            public void onFileBuildStart(final Path sourceFile, final Path outputFile)
            {
                logger.info(sourceFile.normalize() + " --> " + outputFile.normalize() + "...");
            }

            @Override
            public void onNoThrowError(final String message, final Exception e)
            {
                logger.error(message);
                errors.add(e);
            }
        };
    }
}
