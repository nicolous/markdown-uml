/**
 *
 */
package com.brainstormdev.markdown.uml.html.cli;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import com.brainstormdev.markdown.uml.BuildListener;
import com.brainstormdev.markdown.uml.html.HtmlBuilder;

/**
 * Command line utility to generate an HTML document for each markdown file, with support for PlantUML diagram.
 * <p>
 * PlantUML code has to be in indented code blocks. The string {@code @startuml} is used to identify the text code to
 * convert. The whole indented text will be given the PlantUML parser. <br>
 * PlantUML text is expected to be UTF-8 encoded.
 * <p>
 * The output is a stand-alone UTF-8 encoded HTML document. All UML diagrams generated are embedded as SVG images.
 */
final class HtmlCli
{
    /** Show the help message for command line. */
    private static void showHelp(final Options options)
    {
        System.out.println("Generate an HTML document for each markdown file, with support for PlantUML diagram");
        System.out.println();

        final HelpFormatter helpFormat = new HelpFormatter();
        helpFormat.printHelp(HtmlCli.class.getName(), options, true);

        System.out.println();
        System.out.println("PlantUML code has to be in indented code blocks. The string  @startuml is used to "
                        + "identify the text code to transform. The whole indented text will be given the PlantUML "
                        + "parser.");
        System.out.println("PlantUML text is expected to be UTF-8 encoded.");
        System.out.println();
        System.out.println("The output is a stand-alone UTF-8 encoded HTML document. All UML diagrams generated are "
                        + "embedded final as SVG images.");
    }

    public static void main(final String[] args) throws ParseException, IOException
    {
        final Options options = new Options();

        final String optHelp = "h";
        options.addOption(optHelp, "help", false, "Display help");

        final String optSrcDir = "mdSrcDir";
        options.addOption(optSrcDir, true,
                        "Directory that contains the markdown source files. Default is the current directory.");

        final String optTargetDir = "mdTargetDir";
        options.addOption(optTargetDir, true, "Output directory for HTML files. Default is the current directory.");

        final CommandLineParser parser = new DefaultParser();
        final CommandLine cmd = parser.parse(options, args);

        if (cmd.hasOption(optHelp))
            showHelp(options);
        else
        {
            final Path srcDir = Paths.get(cmd.getOptionValue(optSrcDir, "."));
            final Path targetDir = Paths.get(cmd.getOptionValue(optTargetDir, "."));

            final HtmlBuilder builder = new HtmlBuilder(createBuildListener(srcDir));
            builder.build(srcDir, targetDir);
        }
    }

    /**
     * Create a listener for the build.
     * @return A listener that print build steps with {@link System.out} and errors with {@link System.err}.
     */
    private static BuildListener createBuildListener(final Path srcDir)
    {
        return new BuildListener()
        {
            @Override
            public void onSourceDiscovery(final List<Path> sourceFiles)
            {
                System.out.println("Found " + sourceFiles.size() + " markdown files in " + srcDir);
            }

            @Override
            public void onFileBuildStart(final Path sourceFile, final Path outputFile)
            {
                System.out.println(sourceFile.normalize() + " --> " + outputFile.normalize() + "...");
            }

            @Override
            public void onNoThrowError(final String message, final Exception e)
            {
                System.err.println(message);
            }
        };
    }
}
