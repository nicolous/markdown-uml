package com.brainstormdev.markdown.uml.html.maven;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import com.brainstormdev.markdown.uml.BuildListener;
import com.brainstormdev.markdown.uml.html.HtmlBuilder;


/**
 * Generate an HTML document for each markdown file, with support for PlantUML diagram.
 * <p>
 * PlantUML code has to be in indented code blocks. The string {@code @startuml} is used to identify the text code to
 * transform. The whole indented text will be given the PlantUML parser. <br>
 * PlantUML text is expected to be UTF-8 encoded.
 * <p>
 * The output is a stand-alone UTF-8 encoded HTML document. All UML diagrams generated are embedded as SVG images.
 */
@Mojo(name = "html")
final class HtmlMojo extends AbstractMojo
{
    /**
     * Directory that contains the markdown source files.
     */
    @Parameter(property = "mdSrcDir", defaultValue = "${project.build.sourceDirectory}")
    private String srcDir;

    /**
     * Output directory for HTML files.
     */
    @Parameter(property = "mdTargetDir", defaultValue = "${project.build.directory}")
    private String targetDir;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException
    {
        final List<Exception> errors = new ArrayList<>();
        final HtmlBuilder builder = new HtmlBuilder(createBuildListener(errors));

        final Log logger = getLog();
        try
        {
            builder.build(Paths.get(srcDir), Paths.get(targetDir));
        }
        catch (final IOException e)
        {
            throw new MojoFailureException(e.getMessage(), e);
        }

        if (!errors.isEmpty())
        {
            for (final Exception exception : errors)
            {
                logger.error(exception);
            }
            throw new MojoFailureException("See previous exceptions");
        }
    }

    /**
     * Create a listener for the build.
     * @param errors List that will be filled with the no-throw errors (see {@link BuildListener#onNoThrowError(String,
     * Exception)).
     * @return A listener that logs output with {@link Log} and accumulates no-throw errors.
     */
    private BuildListener createBuildListener(final List<Exception> errors)
    {
        final Log logger = getLog();
        return new BuildListener()
        {

            @Override
            public void onSourceDiscovery(final List<Path> sourceFiles)
            {
                logger.info("Found " + sourceFiles.size() + " markdown files in " + srcDir);
            }

            @Override
            public void onFileBuildStart(final Path sourceFile, final Path outputFile)
            {
                logger.info(sourceFile.normalize() + " --> " + outputFile.normalize() + "...");
            }

            @Override
            public void onNoThrowError(final String message, final Exception e)
            {
                logger.error(message);
                errors.add(e);
            }
        };
    }
}
