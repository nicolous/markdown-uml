package com.brainstormdev.markdown.uml;

import java.nio.file.Path;
import java.util.List;

/**
 * Listener of a build.
 * <p>
 * A build consists on transforming a set of markdown files. The output format depends on which program is
 * started and its configuration.
 */
public interface BuildListener
{
    /**
     * The list of source files to transform has been discovered.
     * @param sourceFiles A list of paths for markdown files.
     */
    default void onSourceDiscovery(final List<Path> sourceFiles)
    {
    }

    /**
     * A markdown file is being transformed.
     * @param sourceFile Markdown file path being transformed.
     * @param outputFile The transformation output file.
     */
    default void onFileBuildStart(final Path sourceFile, final Path outputFile)
    {
    }

    /**
     * A markdown file has been transformed.
     * @param sourceFile Markdown file path which has being transformed.
     * @param outputFile The transformation output file.
     */
    default void onFileBuildEnd(final Path sourceFile, final Path outputFile)
    {
    }

    /**
     * An exception occurred but will no be rethrown. May happen if an exception occurs in some third-party class
     * extension where overriden methods can't throw a checked exception.
     * @param message Description of what happened.
     * @param e The said exception.
     */
    default void onNoThrowError(final String message, final Exception e)
    {
    }
}
