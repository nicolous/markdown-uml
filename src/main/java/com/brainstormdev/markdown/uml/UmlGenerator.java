package com.brainstormdev.markdown.uml;

import java.io.IOException;

/**
 * Base interface for an UML generator.
 * <p>
 * Such generator transforms a text representation of an UML diagram into an image.
 */
public interface UmlGenerator
{
    /**
     * Determine if some block's info string indicates that the block contains code this generator can transform.
     * @param infoString <a href="https://spec.commonmark.org/0.28/#info-string">info string</a>
     * as described in the commonmark's spec.
     * Typically used with <a href="https://spec.commonmark.org/0.28/#fenced-code-blocks">Fenced code blocks"</a>.
     * @return true if the info string indicates uml code.
     */
    boolean isInfoStringSupported(String infoString);

    /**
     * Determine if some text represents an UML diagram that this generator can transform.
     * @param uml Text to test.
     * @return true if the input text can be transformed by this generator.
     */
    boolean isSupported(String uml);

    /**
     * Generate SVG code from an UML diagram.
     * @param uml Diagram to transform.
     * @return SVG (XML) representation of the UML input.
     * @throws IOException An error occurred during the transformation.
     */
    byte[] toSvg(String uml) throws IOException;
}
