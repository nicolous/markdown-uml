package com.brainstormdev.markdown.uml.plantuml;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import com.brainstormdev.markdown.uml.UmlGenerator;

import net.sourceforge.plantuml.FileFormat;
import net.sourceforge.plantuml.FileFormatOption;
import net.sourceforge.plantuml.SourceStringReader;


/**
 * UML diagram generator supporting PlantUML code.
 */
public final class PlantUmlGenerator implements UmlGenerator
{
    @Override
    public boolean isInfoStringSupported(String infoString)
    {
        return "plantuml".equals(infoString);
    }

    @Override
    public boolean isSupported(final String code)
    {
        return code.contains("@startuml");
    }

    @Override
    public byte[] toSvg(final String uml) throws IOException
    {
        final SourceStringReader codeParser = new SourceStringReader(uml, "UTF-8");
        try (final ByteArrayOutputStream codeAsBytes = new ByteArrayOutputStream())
        {
            codeParser.generateImage(codeAsBytes, new FileFormatOption(FileFormat.SVG));
            return codeAsBytes.toByteArray();
        }
    }
}
